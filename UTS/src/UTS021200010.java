import java.util.Scanner;
/*
Nama    : Agung Legatanbape AS
NPM     : 021200010
Prodi   : S1 Sistem Informasi
 */
public class UTS021200010 {
    public static void main(String[] args) {
        Scanner input = new Scanner (System.in);
        String hasil, nama, npm;
        int nilai_tugas,nilai_uts,nilai_uas;
        double nilai_akhir;
        System.out.println("=====  Menghitung Nilai Akhir Mahasiswa  =====");
        System.out.print("Masukkan Nama Anda : ");
        nama = input.nextLine();
        System.out.print("Masukkan NPM Anda : ");
        npm = input.nextLine();
        System.out.print("Masukkan Nilai Tugas Anda : ");
        nilai_tugas = input.nextInt();
        System.out.print("Masukkan Nilai UTS Anda : ");
        nilai_uts = input.nextInt();
        System.out.print("Masukkan Nilai UAS Anda : ");
        nilai_uas = input.nextInt();
        System.out.println("===== Data Telah Diinput Sedang Diproses =====");
        System.out.println("Loading 10%....");
        System.out.println("Loading 17%....");
        System.out.println("Loading 45%....");
        System.out.println("Loading 68%....");
        System.out.println("Loading 81%....");
        System.out.println("Loading 99%....");
        System.out.println("Prossesing%....");
        
        nilai_tugas = (int)(nilai_tugas*0.40);
        nilai_uts = (int)(nilai_uts*0.25);
        nilai_uas = (int)(nilai_uas*0.35);
        nilai_akhir =nilai_tugas+nilai_uts+nilai_uas;
        if(nilai_akhir >= 85 && nilai_akhir <= 100){
            hasil =("Dinyatakan Lulus Dengan Grade A");
        }else if(nilai_akhir >= 70 && nilai_akhir <85){
            hasil ="Dinyatakan Lulus Dengan Grade B";
        }else if(nilai_akhir >= 60 && nilai_akhir <70){
            hasil ="Dinyatakan Lulus Dengan Grade C";
        }else if(nilai_akhir >= 50 && nilai_akhir <60){
            hasil ="Dinyatakan Tidak Lulus Dengan Grade D";
        }else{
            hasil ="Dinyatakan Tidak Lulus Dengan Grade E";
        }
        System.out.println("===== Hasil Nilai Akhir Mahasiswa =====");
        System.out.println("Atas Nama : "+nama);
        System.out.println("Dengan NPM : "+npm);
        System.out.println("Nilai Akhir : "+nilai_akhir);
        System.out.println(hasil);
        System.out.println("=======================================");
    }
 
}
